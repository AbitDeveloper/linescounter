﻿using System;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Tester
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.Write("Enter extensions separated by spaces: ");

			string extensions = Console.ReadLine();

			string[] exts;
			if(string.IsNullOrWhiteSpace(extensions))
			{
				Console.WriteLine("You didnt entered any extensions.");
				return;
			}

			exts = extensions.Split(' ').Where(delegate(string arg) 
			{
				return !string.IsNullOrEmpty(arg);
			}).ToArray();

			Console.WriteLine("Searchin for files.");

			string directory = args.Length == 0 ? Environment.CurrentDirectory : string.Join(" ", args);

			List<string> paths = Paths(directory, exts);

			if(paths == null || paths.Count == 0)
			{
				Console.WriteLine("No file found.");
				return;
			}

			Console.WriteLine("Found {0} files. Let's count it's lines...", paths.Count);
			Console.WriteLine("This might take a while. I will print (only for you) one star for each file.");

			long lines = 0;
			foreach(string s in paths)
			{
				using(StreamReader sr = new StreamReader(s))
				{
					while(sr.ReadLine() != null && !sr.EndOfStream)
						lines++;
				}
				Console.Write('*');
			}
			Console.WriteLine();

			Console.WriteLine("Found {0} lines in {1} files.", lines, paths.Count);
			Console.WriteLine("Press any key to continue...");
			Console.Read();
		}

		public static List<string> Paths(string directory, string[] exts)
		{
			if(!Directory.Exists(directory))
				return null;
			List<string> list = Directory.GetFiles(directory).Where(delegate(string arg1)
			{
				return exts.Where(delegate(string arg2) 
				{
					return arg1.EndsWith(arg2);
				}).Count() > 0;
			}).ToList();
			foreach(string d in Directory.GetDirectories(directory))
				list.AddRange(Paths(d, exts));
			return list;
		}
	}
}